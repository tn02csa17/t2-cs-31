blockchain = []


def get_last_blockchain_value():
    if len(blockchain) < 1:
        return None
    return blockchain[-1]


def add_data(data, last_data=[1]):
    if last_data == None:
        last_data = [1]
    blockchain.append([last_data, data])


def get_data():
    user_input = input('Patient data: ')
    return user_input


def get_user_choice():
    user_input = input('Your choice: ')
    return user_input


def print_blockchain_elements():
    for block in blockchain:
        print('Block output')
        print(block)


def verify_chain():
    block_index = 0
    is_valid = True
    for block in blockchain:
        if block_index == 0:
            block_index += 1
            continue
        elif block[0] == blockchain[block_index - 1]:
            is_valid = True
        else:
            is_valid = False
            break
        block_index += 1
    return is_valid

while True:
    print('Please choose')
    print('1: Patient Data')
    print('2: Output Data')
    print('q: Quit')
    user_choice = get_user_choice()
    if user_choice == '1':
        history = get_data()
        add_data(history, get_last_blockchain_value())
    elif user_choice == '2':
        print_blockchain_elements()
    elif user_choice == 'q':
        break
    else:
        print('Input was invalid, please pick a value from the list!')
    if not verify_chain():
        print('Invalid blockchain!')
        break


print('Done!')
