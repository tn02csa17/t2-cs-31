//Sign UP
const signupForm = document.querySelector('#signup-form');
signupForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const email = signupForm['signup-email'].value;
    const password = signupForm['signup-password'].value;

    auth.createUserWithEmailAndPassword(email, password)
    .then(cred => {
        return db.collection('users').doc(cred.user.uid).set({
            name: signupForm['signup-name'].value  
        });
    })
    .then(() => {
        signupForm.reset();
        setTimeout(1000);
        window.location.href = './main.html';
    });
    $('#modal-signup').modal('hide');
});


//Log In
const loginForm = document.querySelector('#login-form');
loginForm.addEventListener('submit', (e) => {
    e.preventDefault();

    const email = loginForm['login-email'].value;
    const password = loginForm['login-password'].value;

    auth.signInWithEmailAndPassword(email, password).then(cred =>{
        console.log(cred.user);
        loginForm.reset();
      });
});

//Authentication State

auth.onAuthStateChanged(function(user) {
    if (user) {
      // User is signed in.)
    }
  });